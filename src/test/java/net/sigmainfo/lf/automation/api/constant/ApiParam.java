package net.sigmainfo.lf.automation.api.constant;

import org.springframework.stereotype.Component;

/**
 * Created by           : Shaishav.s on 08-02-2017.
 * Test class           : ApiParam.java
 * Description          : Contains all placeholders declared within the test package
 * Includes             : 1. Declares the place holders being used
 *                        2. Contains getter and setter methods
 */

/**
 *  ALL THE VARIABLES TO BE DECLARED IN THIS CLASS
 */
@Component
public class ApiParam {


}
